//
//  TCoreData.h
//  Combustonator
//
//  Created by Vinicius Tonon on 1/13/14.
//  Copyright (c) 2014 Vinicius Tonon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCoreData : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveData:(NSString *)CDName forValues:(NSArray *)values forKeys:(NSArray *)keys;
- (NSArray *)retData:(NSString *)CDName;
- (void)deleteData:(NSManagedObject *)obj;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

+ (id)sharedManager;

@end
